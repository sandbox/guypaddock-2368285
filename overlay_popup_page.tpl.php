<div id="page-wrapper">
  <div id="page">
    <div id="main-wrapper">
      <div id="main" class="clearfix">
        <div id="content" class="column">
          <div class="section">
            <a id="main-content"></a>
            <?php
              $have_messages = (count(drupal_get_messages(NULL, FALSE)) != 0);
              $have_errors   = (count(drupal_get_messages('error', FALSE)) != 0);
              $have_warnings = (count(drupal_get_messages('warning', FALSE)) != 0);

              
              $messages = theme('status_messages');
              
              if (!empty($messages)) {
                print render($messages);
              }

              // Hide main content on success.
              if (!$have_messages || $have_errors || $have_warnings) {
                print render($page['content']);
              }
              else {
                $site_name = variable_get('site_name', "site");
                ?>
                <a href="#" id="close-overlay">Return to <?php print $site_name; ?></a>
                <?php
              }
            ?>
            <script type="text/javascript">
              (function ($) {
                $("#close-overlay").click(function() {
                  parent.jQuery.colorbox.close();
                });
              })(jQuery);
            </script>
          </div>
        </div><!-- /.section -->
        <!-- /#content -->
      </div><!-- /#main -->
    </div><!-- /#main-wrapper -->
  </div><!-- /#page -->
</div><!-- /#page-wrapper -->